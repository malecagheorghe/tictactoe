package TicTacToe;

import java.util.Scanner;

public class Person implements Player{

    //set up the constructor
    Person(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    //for taking input from the terminal
    private Scanner scan = new Scanner(System.in);

    //define which player is this object representing
    private int playerNumber;

    //parameters:   the game board
    //return:   takes user input and changes a cell in the game board
    public void makeMove(GameBoard gameBoard) {
        int column, row;

        //check that the chosen cell is empty
        do {
            //check that the user inserts only numbers from 1 to 3
            do {
                if (playerNumber == 1) {
                    System.out.print("\nPlayer 'X', enter your move (Starting from Top Left corner: row[1-3] column[1-3]) : ");
                } else {
                    System.out.print("\nPlayer 'O', enter your move (Starting from Top Left corner: row[1-3] column[1-3]) : ");
                }
                row = scan.nextInt() - 1;
                column = scan.nextInt() - 1;

                System.out.println();

            } while ((row < 0 || row > 2) || (column < 0 || column > 2));
        } while (gameBoard.gameBoard[row][column] == 1 || gameBoard.gameBoard[row][column] == 2);

        gameBoard.gameBoard[row][column] = playerNumber;
    }
}
