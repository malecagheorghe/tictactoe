package TicTacToe;

public class GameBoard {

    //initiate the game board as an instance variable
    int[][] gameBoard = new int[3][3];


    //parameters:   the game board
    //return:   the number of filled cells
    int countFilledCells() {
        int result = 0;

        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                if (gameBoard[row][column] != 0) {
                    result++;
                }
            }
        }

        return result;
    }

    //parameters:   the game board at the current status
    //return:   sout the XO game board at the current status
    void showGameBoard() {
        for (int i = 0; i < 3; i++) {
            //iterate through each row
            if (i > 0) {
                System.out.println("---------");
            }
            for (int j = 0; j < 3; j++) {
                //iterate through each column
                if (j > 0) {
                    System.out.print(" | ");
                }

                //show the content of the cell
                switch (gameBoard[i][j]) {
                    case 0:
                        System.out.print("*");
                        break;
                    case 1:
                        System.out.print("X");
                        break;
                    case 2:
                        System.out.print("O");
                        break;
                }
            }
            System.out.println();
        }
    }

    //parameters:   the game board at the current status
    //return:   0 = game continues; 1, 2 = this player won; -1 = there is a tie;
    int checkGameBoard(int[][] gameBoard) {

        //check the diagonals
        if (gameBoard[0][0] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][2] && gameBoard[2][2] != 0) {
            return gameBoard[0][0];
        }
        if (gameBoard[0][2] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][0] && gameBoard[2][0] != 0) {
            return gameBoard[0][2];
        }

        //check straight lines
        for (int i = 0; i < 3; i++) {
            //check the rows
            if (gameBoard[i][0] == gameBoard[i][1] && gameBoard[i][1] == gameBoard[i][2] && gameBoard[i][2] != 0) {
                return gameBoard[i][0];
            }
            //check the columns
            if (gameBoard[0][i] == gameBoard[1][i] && gameBoard[1][i] == gameBoard[2][i] && gameBoard[2][i] != 0) {
                return gameBoard[0][i];
            }
        }

        //if the board is not full and none of the players won yet
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (gameBoard[i][j] == 0) {
                    return 0;
                }
            }
        }

        //if the program arrived here it means there is no winner and there are no more moves available
        return -1;
    }

//----------------------------------------------------------------------------------------------------------------------


    //initiate an instance variable to host the graph of all possible moves
    int[][] graphBoard = new int[3][3];


    //parameters:   the graph representation of the current possible moves on the game board
    //return:   returns the cell with the largest amount of possible moves [row; column]
    int[] checkGraphBoard(int[][] gameBoard, int[][] graphBoard) {

        //fill the graph board
        fillGraphBoard(gameBoard, graphBoard);

        //find the cell with the most possible moves

        int[] maxCell = findMaxCell(gameBoard, graphBoard);

        //clear the graph board
        clearGraphBoard(graphBoard);

        return maxCell;
    }

    //parameters:
    //return:
    private int[] findMaxCell(int[][] gameBoard, int[][] graphBoard) {
        //initialize the cell to an empty cell if there is such a cell
        int[] maxCell = {-1, -1}; //[row, column]
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                if (gameBoard[row][column] == 0) {
                    maxCell[0] = row;
                    maxCell[1] = column;
                }
            }
        }

        //find the cell with the largest value which represents the amount of lines benefiting from this cell
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                if (graphBoard[maxCell[0]][maxCell[1]] < graphBoard[row][column]) {
                    maxCell[0] = row;
                    maxCell[1] = column;
                }
            }
        }

        return maxCell;
    }

    //parameters:   the graph representation of the current possible moves on the game board
    //return:   clears the graphBoard
    private void clearGraphBoard(int[][] graphBoard) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                graphBoard[i][j] = 0;
            }
        }
    }

    //parameters:   the graph representation of the current possible moves on the game board
    //return:   fills the graph board with the amount of possible moves for each cell
    private void fillGraphBoard(int[][] gameBoard, int[][] graphBoard) {

        //run through the graph and write down how big of impact has each cell
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                //in case that the current cell on the game board is empty
                if (gameBoard[row][column] == 0) {

                    //for each check increase the value of the current cell if a match was found
                    graphBoard[row][column] += checkColumnOneThird(row, column);

                    graphBoard[row][column] += checkRowOneThird(row, column);

                    graphBoard[row][column] += checkDiagonalsOneThird(row, column);

                }
            }
        }
    }


    //check 1/3rd for the graph board

    //parameters:   the graph board and a cell to check
    //return:   returns true if there is a 1/3rd case for the row
    private int checkRowOneThird(int row, int column) {
        if (column == 0) {
            if (gameBoard[row][1] == 0 ^ gameBoard[row][2] == 0) {
                return 1;
            }
        } else if (column == 1) {
            if (gameBoard[row][0] == 0 ^ gameBoard[row][2] == 0) {
                return 1;
            }
        } else if (column == 2) {
            if (gameBoard[row][1] == 0 ^ gameBoard[row][0] == 0) {
                return 1;
            }
        }

        //default
        return 0;
    }

    //parameters:   the graph board and a cell to check
    //return:   returns true if there is a 1/3rd case for the column
    private int checkColumnOneThird(int row, int column) {
        if (row == 0) {
            if (gameBoard[1][column] == 0 ^ gameBoard[2][column] == 0) {
                return 1;
            }
        } else if (row == 1) {
            if (gameBoard[0][column] == 0 ^ gameBoard[2][column] == 0) {
                return 1;
            }
        } else if (row == 2) {
            if (gameBoard[1][column] == 0 ^ gameBoard[0][column] == 0) {
                return 1;
            }
        }

        //default
        return 0;
    }

    //parameters:   the graph board and a cell to check
    //return:   returns true if there is a 1/3rd case for the diagonals
    private int checkDiagonalsOneThird(int row, int column) {
        int result = 0;

        //leftwards leaning diagonal
        if (row == column) {
            if (row == 0) {
                if (gameBoard[1][1] == 0 ^ gameBoard[2][2] == 0) {
                    result++;
                }
            } else if (row == 1) {
                if (gameBoard[0][0] == 0 ^ gameBoard[2][2] == 0) {
                    result++;
                }
            } else if (row == 2) {
                if (gameBoard[1][1] == 0 ^ gameBoard[0][0] == 0) {
                    result++;
                }
            }
        }

        //rightwards leaning diagonal
        if (row + column == 2) {
            if (row == 0) {
                if (gameBoard[1][1] == 0 ^ gameBoard[2][0] == 0) {
                    result++;
                }
            } else if (row == 1) {
                if (gameBoard[0][2] == 0 ^ gameBoard[2][0] == 0) {
                    result++;
                }
            } else if (row == 2) {
                if (gameBoard[1][1] == 0 ^ gameBoard[0][2] == 0) {
                    result++;
                }
            }
        }

        //default
        return result;
    }
}
