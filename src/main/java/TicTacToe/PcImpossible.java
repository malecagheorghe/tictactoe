package TicTacToe;

public class PcImpossible extends Pc implements Player{

    //define which player is this object representing
    private int playerNumber;

    //set up the constructor
    PcImpossible(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    //parameters:   the current status of the game board
    //return:   checks 2/3rds, checks 1/3rds and if none then does special moves if the game just began. Otherwise makes a random move
    public void makeMove(GameBoard gameBoard) {

        if (playerNumber == 1) {
            System.out.println("Player 'X' is entering his move.");
        } else {
            System.out.println("Player 'O' is entering his move.");
        }

        int row, column;

        //run 2/3rds check
        int[] coordinates = checkTwoThirds(gameBoard, playerNumber);  //coordinates[row, column]
        if (coordinates[0] != -1) {
            //check if there is a 2/3rds solution

            //use the 2/3rd solution
            row = coordinates[0];
            column = coordinates[1];

            //use current coordinates and win or block the other player
            gameBoard.gameBoard[row][column] = playerNumber;
            return;

        } else {
            //if there is no 2/3rds check for 1/3rd

            //use the graph board
            coordinates = gameBoard.checkGraphBoard(gameBoard.gameBoard, gameBoard.graphBoard);

            //if the graph did not return a valid cell then choose a random cell
            if (coordinates[0] == -1) {
                coordinates = generateRandomCell(gameBoard);
            }

            //the coordinates will come from the graph or from a random cell
            row = coordinates[0];
            column = coordinates[1];
        }


        //in case the game just began, override the graph solution and do the following moves
        coordinates = checkFirstMove(gameBoard, playerNumber);

        //in case the game is at the beginning override the program solution
        if (coordinates[0] != -1) {
            row = coordinates[0];
            column = coordinates[1];
        }

        //the coordinates will come from the 2/3rds or 1/3rd checks if the game is not at its beginning. Otherwise the coordinates will be generated according to special moves which are specific for the beginning of the game
        gameBoard.gameBoard[row][column] = playerNumber;
    }
}
