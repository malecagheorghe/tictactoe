package TicTacToe;

public class PcEasy extends Pc implements Player {

    //define which player is this object representing
    private int playerNumber;

    //set up the constructor
    PcEasy(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    //parameters:   game board
    //return:   pick a random cell
    public void makeMove(GameBoard gameBoard) {

        if (playerNumber == 1) {
            System.out.println("\nPlayer 'X' is entering his move.");
        } else {
            System.out.println("\nPlayer 'O' is entering his move.");
        }

        //pick a random empty cell
        int[] coordinates = generateRandomCell(gameBoard);

        //assign the chosen cell to the current player
        gameBoard.gameBoard[coordinates[0]][coordinates[1]] = playerNumber;
    }
}
