package TicTacToe;

public interface Player {
    void makeMove(GameBoard gameBoard);
}