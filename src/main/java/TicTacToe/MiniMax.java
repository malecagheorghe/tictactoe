package TicTacToe;

public class MiniMax implements Player {

    //define which player is this object representing
    private int playerNumber;

    //set up the constructor
    public MiniMax(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    //------------------------------------------------------------------------------------------------------------------
    //parameters: the game board, the player number
    //return:
    public void makeMove(GameBoard game) {

        int[][] graphBoardFrame = {{999999, 999999, 999999}, {999999, 999999, 999999}, {999999, 999999, 999999}};
        int otherPlayer = (playerNumber == 1) ? 2 : 1;
        int depth = 0;

        //run recursive on all the empty cells
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                if (game.gameBoard[row][column] == 0) {
                    //if the current cell is empty

                    //make a new frame of the game board and give it as parameter for the next recursive call
                    int[][] gameBoardFrame2 = copyBoard(game.gameBoard);
                    gameBoardFrame2[row][column] = playerNumber;

                    //save the winning depth for this branch in a bi-dimensional array
                    graphBoardFrame[row][column] = makeMoveRecursive(game, gameBoardFrame2, otherPlayer, ++depth, -1);
                }
            }
        }

        //find the minimum depth to win
        int[] result = {0, 0};
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                result = (graphBoardFrame[row][column] < graphBoardFrame[result[0]][result[1]]) ? new int[]{row, column} : result;
            }
        }

        //make the move
        if (playerNumber == 1) {
            System.out.println("\nPlayer 'X' is entering his move.");
        } else {
            System.out.println("\nPlayer 'O' is entering his move.");
        }

        game.gameBoard[result[0]][result[1]] = playerNumber;
    }

    //------------------------------------------------------------------------------------------------------------------
    //prep for the recursive call
    //parameters: gameBoard and playerNumber
    //return:   registers the move into the gameBoard
    private int makeMoveRecursive(GameBoard game, int[][] gameBoardFrame, int playerNumber, int depth, int parentAlpha) {

        int otherPlayer = (playerNumber == 1) ? 2 : 1;
        int minDepth = 999999;
        int maxDepth = 0;

        int alpha = -1;

        //base case
        int winner = game.checkGameBoard(gameBoardFrame);   //return:   0 = game continues; 1, 2 = this player won; -1 = there is a tie;

        if (winner == -1 || winner == this.playerNumber) {
            //if it's a win you return the depth (stop checking for other options in this branch) and this depth is written in the graph one level up
            return depth;
        } else if (winner != 0) {
            //if the opponent wins before the player on this path, stop checking and return a non option
            return minDepth;
        }

        //run recursive for all the empty cells
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                if (gameBoardFrame[row][column] == 0) {
                    //if the current cell is empty

                    //make a new frame of the game board and give it as parameter for the next recursive call
                    int[][] gameBoardFrame2 = copyBoard(gameBoardFrame);
                    gameBoardFrame2[row][column] = playerNumber;

                    int temp = makeMoveRecursive(game, gameBoardFrame2, otherPlayer, ++depth, alpha);

                    minDepth = Math.min(minDepth, temp);
                    maxDepth = Math.max(maxDepth, temp);

                    //update alpha
                    alpha = (playerNumber == this.playerNumber) ? minDepth : maxDepth;

                    //base case for alpha beta
                    if (parentAlpha != -1) {

                        if (playerNumber == this.playerNumber) {
                            //current node wants to minimize, parent node wants to maximize
                            if (alpha < parentAlpha) {
                                return alpha;
                            }
                        } else {
                            //current node wants to maximize, parent node wants to minimize
                            if (alpha > parentAlpha) {
                                return alpha;
                            }
                        }
                    }
                }
            }
        }

        //return the best case scenario for this child
        return alpha;
    }

    //parameters:   bi-dimensional array
    //return:   bi-dimensional array with same value, but different address
    private int[][] copyBoard(int[][] gameBoard) {
        int[][] gameBoardFrame = new int[3][3];

        //run recursive for all the cells
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                gameBoardFrame[row][column] = gameBoard[row][column];
            }
        }

        return gameBoardFrame;
    }

}
