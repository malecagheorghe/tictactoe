package TicTacToe;

public class PcHard extends Pc implements Player {

    //define which player is this object representing
    private int playerNumber;

    //set up the constructor
    PcHard(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    //parameters:   the current status of the game board
    //return:   checks for lines which are to be completed and if none are found, the pc looks for lines it has already started and which are still empty and if none again, then it chooses a random cell
    public void makeMove(GameBoard gameBoard) {
        int row, column;

        if (playerNumber == 1) {
            System.out.println("\nPlayer 'X' is entering his move.");
        } else {
            System.out.println("\nPlayer 'O' is entering his move.");
        }

        //run 2/3rds check
        int[] coordinates = checkTwoThirds(gameBoard, playerNumber);  //coordinates[row, column]

        //check if there is a 2/3rds solution
        if (coordinates[0] != -1) {
            //use the 2/3rd solution
            row = coordinates[0];
            column = coordinates[1];
        } else {

            coordinates = gameBoard.checkGraphBoard(gameBoard.gameBoard, gameBoard.graphBoard);

            //if the graph did not return a valid cell then choose a random cell
            if (coordinates[0] == -1) {
                coordinates = generateRandomCell(gameBoard);
            }

            //the coordinates will come from the graph or from a random cell
            row = coordinates[0];
            column = coordinates[1];
        }

        gameBoard.gameBoard[row][column] = playerNumber;
    }
}
