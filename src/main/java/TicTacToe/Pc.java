package TicTacToe;

import java.util.Random;

public class Pc{
    //------------------------------------------------------------------------------------------------------------------
    //                        FUNCTION TO BE INHERITED AND USED BY THE SUB-CLASSES
    //------------------------------------------------------------------------------------------------------------------

    //sub-functions for the computer player in hard mode
    protected int[] checkFirstMove(GameBoard gameBoard, int playerNumber) {
        //initialize the local variables
        int[] coordinates = {-1, -1};


        int numberOfFilledCells = gameBoard.countFilledCells();

        //FIRST MOVE
        if (numberOfFilledCells == 0) {
            //if this player starts, pick the Left Top corner
            coordinates[0] = coordinates[1] = 0;
        }

        //SECOND MOVE
        if (numberOfFilledCells == 1) {
            //if this player is the second player and this is the second move pick the center if possible, otherwise pick the Top Left corner
            if (gameBoard.gameBoard[1][1] == 0) {
                //pick the center if it is free
                coordinates[0] = coordinates[1] = 1;
            } else {
                //pick the Top Left corner if the center is taken
                coordinates[0] = coordinates[1] = 0;
            }
        }

        //FOURTH MOVE
        if (numberOfFilledCells == 3 && gameBoard.gameBoard[1][1] == playerNumber) {
            //in case the other player picked the corner as first move and the pc picked the center after that
            //pick a cell from the cross towards the area with most filled cells

            int left = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 2; j++) {
                    left += gameBoard.gameBoard[i][j];
                }
            }

            int bottom = 0;
            for (int i = 1; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    bottom += gameBoard.gameBoard[i][j];
                }
            }

            int right = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 1; j < 3; j++) {
                    right += gameBoard.gameBoard[i][j];
                }
            }

            int top = 0;
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 3; j++) {
                    top += gameBoard.gameBoard[i][j];
                }
            }

            //find the maximum between the four sides
            int max = left;
            max = Math.max(max, bottom);
            max = Math.max(max, right);
            max = Math.max(max, top);

            //decide on which side to pick the cell
            if (max == left && gameBoard.gameBoard[1][0] == 0) {
                coordinates[0] = 1;
                coordinates[1] = 0;
            } else if (max == bottom && gameBoard.gameBoard[2][1] == 0) {
                coordinates[0] = 2;
                coordinates[1] = 1;
            } else if (max == right && gameBoard.gameBoard[1][2] == 0) {
                coordinates[0] = 1;
                coordinates[1] = 2;
            } else if (max == top && gameBoard.gameBoard[0][1] == 0) {
                coordinates[0] = 0;
                coordinates[1] = 1;
            }
        }

        return coordinates;
    }

    protected int[] generateRandomCell(GameBoard gameBoard) {
        Random randInt = new Random();
        int[] coordinates = {0, 0}; //[row, column]

        do {

            //assign random values
            coordinates[0] = randInt.nextInt(3);
            coordinates[1] = randInt.nextInt(3);

        } while (gameBoard.gameBoard[coordinates[0]][coordinates[1]] == 1 || gameBoard.gameBoard[coordinates[0]][coordinates[1]] == 2);

        return coordinates;
    }


    //check 2/3rds

    //parameters:   the current status of the game board and the current player
    //return:   [row, column]  checks if the current player has a line which ca be won in the next move and if none it checks if the other player has such a line and returns the empty cell on this line (in both cases)
    protected int[] checkTwoThirds(GameBoard gameBoard, int playerNumber) {

        //coordinates[row, column]
        int[] coordinates = {-1, -1};

        //check current player's 2/3rd
        //check the diagonals
        coordinates = checkMyDiagonalsTwoThirds(gameBoard, playerNumber);
        if (coordinates[0] != -1) {
            return coordinates;
        }
        //check the rows and the columns
        for (int i = 0; i < 3; i++) {
            coordinates = checkMyRowTwoThirds(gameBoard, i, playerNumber);
            if (coordinates[0] != -1) {
                return coordinates;
            }
            coordinates = checkMyColumnTwoThirds(gameBoard, i, playerNumber);
            if (coordinates[0] != -1) {
                return coordinates;
            }
        }

        //check any 2/3rds
        //check the diagonals
        coordinates = checkAnyDiagonalsTwoThirds(gameBoard);
        if (coordinates[0] != -1) {
            return coordinates;
        }
        //check the rows and the columns
        for (int i = 0; i < 3; i++) {
            coordinates = checkAnyRowTwoThirds(gameBoard, i);
            if (coordinates[0] != -1) {
                return coordinates;
            }
            coordinates = checkAnyColumnTwoThirds(gameBoard, i);
            if (coordinates[0] != -1) {
                return coordinates;
            }
        }

        //return the initialized array if no 2/3rds situation was found
        return coordinates;
    }

    //parameters:   the current status of the game board and the current player
    //return:   [row, column]  checks if the current player has any of the two diagonals filled by 2/3rds and returns the empty cell
    protected int[] checkMyDiagonalsTwoThirds(GameBoard gameBoard, int playerNumber) {
        int[] coordinates = {-1, -1};

        //diagonal leaning towards left
        {
            int counter = 0;
            boolean isAvailable = false;
            //iterate through the cells on the diagonal
            for (int i = 0; i < 3; i++) {
                if (gameBoard.gameBoard[i][i] == playerNumber) {
                    //if the current cell is empty
                    counter++;
                } else if (gameBoard.gameBoard[i][i] == 0) {
                    //if the current cell has playerNumber in it
                    isAvailable = true;
                }
            }

            //check if this diagonal is two thirds filled by the current player
            if ((counter == 2) && (isAvailable)) {

                //find an empty cell and return it
                if (gameBoard.gameBoard[0][0] == 0) {
                    coordinates[0] = coordinates[1] = 0;
                    return coordinates;
                } else if (gameBoard.gameBoard[2][2] == 0) {
                    coordinates[0] = coordinates[1] = 2;
                    return coordinates;
                } else {
                    coordinates[0] = coordinates[1] = 1;
                    return coordinates;
                }
            }
        }


        //diagonal leaning towards right
        {
            int counter = 0;
            boolean isAvailable = false;
            //iterate through the cells on the diagonal
            for (int i = 0; i < 3; i++) {
                if (gameBoard.gameBoard[i][2 - i] == playerNumber) {
                    //if the current cell is empty
                    counter++;
                } else if (gameBoard.gameBoard[i][2 - i] == 0) {
                    //if the current cell has playerNumber in it
                    isAvailable = true;
                }
            }

            //check if this diagonal is two thirds filled by the current player
            if ((counter == 2) && (isAvailable)) {

                //find an empty cell and return it
                if (gameBoard.gameBoard[0][2] == 0) {
                    coordinates[0] = 0;
                    coordinates[1] = 2;
                    return coordinates;
                } else if (gameBoard.gameBoard[2][0] == 0) {
                    coordinates[0] = 2;
                    coordinates[1] = 0;
                    return coordinates;
                } else {
                    coordinates[0] = coordinates[1] = 1;
                    return coordinates;
                }
            }
        }

        //if no proper cell found, return default {-1, -1}
        return coordinates;
    }

    //parameters:   the current status of the game board
    //return:   [row, column]  checks if any player has 2/3rds of any of the diagonals filled and returns the empty cell
    protected int[] checkAnyDiagonalsTwoThirds(GameBoard gameBoard) {
        int[] coordinates = {-1, -1};

        //diagonal leaning towards left
        {
            //iterate through the cells on the diagonal
            for (int i = 0; i < 3; i++) {
                if (gameBoard.gameBoard[i][i] == 0) {
                    //the current cell is empty

                    //check if the other two cells are filled by the same player
                    if (i == 0) {
                        if ((gameBoard.gameBoard[1][1] == gameBoard.gameBoard[2][2]) && (gameBoard.gameBoard[1][1] != 0)) {
                            coordinates[0] = coordinates[1] = i;
                            return coordinates;
                        }
                    }
                    if (i == 1) {
                        if ((gameBoard.gameBoard[0][0] == gameBoard.gameBoard[2][2]) && (gameBoard.gameBoard[0][0] != 0)) {
                            coordinates[0] = coordinates[1] = i;
                            return coordinates;
                        }
                    }
                    if (i == 2) {
                        if ((gameBoard.gameBoard[1][1] == gameBoard.gameBoard[0][0]) && (gameBoard.gameBoard[1][1] != 0)) {
                            coordinates[0] = coordinates[1] = i;
                            return coordinates;
                        }
                    }
                }
            }
        }


        //diagonal leaning towards right
        {
            //iterate through the cells on the diagonal
            for (int i = 0; i < 3; i++) {
                if (gameBoard.gameBoard[i][2 - i] == 0) {
                    //the current cell is empty

                    //check if the other two cells are filled by the same player
                    if (i == 0) {
                        if ((gameBoard.gameBoard[1][1] == gameBoard.gameBoard[2][0]) && (gameBoard.gameBoard[1][1] != 0)) {
                            coordinates[0] = 0;
                            coordinates[1] = 2;
                            return coordinates;
                        }
                    }
                    if (i == 1) {
                        if ((gameBoard.gameBoard[0][2] == gameBoard.gameBoard[2][0]) && (gameBoard.gameBoard[0][2] != 0)) {
                            coordinates[0] = coordinates[1] = 1;
                            return coordinates;
                        }
                    }
                    if (i == 2) {
                        if ((gameBoard.gameBoard[1][1] == gameBoard.gameBoard[0][2]) && (gameBoard.gameBoard[1][1] != 0)) {
                            coordinates[0] = 2;
                            coordinates[1] = 0;
                            return coordinates;
                        }
                    }
                }
            }
        }


        //if no proper cell found, return default {-1, -1}
        return coordinates;
    }

    //parameters:   the current status of the game board, the rwo to be checked, and the current player
    //return:   [row, column]  checks if the current row is filled up to 2/3rds by the current player and returns the third empty cell
    protected int[] checkMyRowTwoThirds(GameBoard gameBoard, int row, int playerNumber) {
        int[] coordinates = {-1, -1};

        int counter = 0;
        boolean isAvailable = false;

        //iterate through the cells on the row
        for (int i = 0; i < 3; i++) {
            if (gameBoard.gameBoard[row][i] == playerNumber) {
                //if the current cell is empty
                counter++;
            } else if (gameBoard.gameBoard[row][i] == 0) {
                //if the current cell has playerNumber in it
                isAvailable = true;
            }
        }

        //check if this row is one third filled by the current player
        if ((counter == 2) && (isAvailable)) {

            //all of the following 3 potential results have the same row value
            coordinates[0] = row;

            //find an empty cell and return it
            if (gameBoard.gameBoard[row][0] == 0) {
                coordinates[1] = 0;
                return coordinates;
            } else if (gameBoard.gameBoard[row][2] == 0) {
                coordinates[1] = 2;
                return coordinates;
            } else {
                coordinates[1] = 1;
                return coordinates;
            }
        }

        //if this row is not filled on 1/3rd by the current player return the default value of {-1, -1}
        return coordinates;
    }

    //parameters:   the current status of the game board and the row to be checked
    //return:   [row, column]  checks if the current row is filled up to 2/3rds by the same player and returns the third empty cell
    protected int[] checkAnyRowTwoThirds(GameBoard gameBoard, int row) {
        int[] coordinates = {-1, -1};

        //iterate through the cells on the row
        for (int column = 0; column < 3; column++) {
            if (gameBoard.gameBoard[row][column] == 0) {
                //if the current cell is empty

                //check that the other two cells are filled by the same player
                if (column == 0) {
                    if ((gameBoard.gameBoard[row][1] == gameBoard.gameBoard[row][2]) && (gameBoard.gameBoard[row][2] != 0)) {
                        coordinates[0] = row;
                        coordinates[1] = column;
                        //return coordinates;
                        break;
                    }
                }
                if (column == 1) {
                    if ((gameBoard.gameBoard[row][0] == gameBoard.gameBoard[row][2]) && (gameBoard.gameBoard[row][2] != 0)) {
                        coordinates[0] = row;
                        coordinates[1] = column;
                        //return coordinates;
                        break;
                    }
                }
                if (column == 2) {
                    if ((gameBoard.gameBoard[row][1] == gameBoard.gameBoard[row][0]) && (gameBoard.gameBoard[row][0] != 0)) {
                        coordinates[0] = row;
                        coordinates[1] = column;
                        //return coordinates;
                        break;
                    }
                }
            }
        }

        //if this row is not filled on 1/3rd by the current player return the default value of {-1, -1}
        return coordinates;
    }

    //parameters:   the current status of the game board, the column to be checked, and the current player
    //return:   [row, column]  checks if the current column is filled up to 2/3rds by the current player and returns the third empty cell
    protected int[] checkMyColumnTwoThirds(GameBoard gameBoard, int column, int playerNumber) {
        int[] coordinates = {-1, -1};

        int counter = 0;
        boolean isAvailable = false;

        //iterate through the cells on the row
        for (int i = 0; i < 3; i++) {
            if (gameBoard.gameBoard[i][column] == playerNumber) {
                //if the current cell is empty
                counter++;
            } else if (gameBoard.gameBoard[i][column] == 0) {
                //if the current cell has playerNumber in it
                isAvailable = true;
            }
        }

        //check if this row is one third filled by the current player
        if ((counter == 2) && (isAvailable)) {

            //all of the following 3 potential results have the same row value
            coordinates[1] = column;

            //find an empty cell and return it
            if (gameBoard.gameBoard[0][column] == 0) {
                coordinates[0] = 0;
                return coordinates;
            } else if (gameBoard.gameBoard[2][column] == 0) {
                coordinates[0] = 2;
                return coordinates;
            } else {
                coordinates[0] = 1;
                return coordinates;
            }
        }

        //if this row is not filled on 1/3rd by the current player return the default value of {-1, -1}
        return coordinates;
    }

    //parameters:   the current status of the game board and the column to be checked
    //return:   [row, column]  checks if the current column is filled up to 2/3rds by the same player and returns the third empty cell
    protected int[] checkAnyColumnTwoThirds(GameBoard gameBoard, int column) {
        int[] coordinates = {-1, -1};

        //iterate through the cells on the row
        for (int row = 0; row < 3; row++) {
            if (gameBoard.gameBoard[row][column] == 0) {
                //if the current cell is empty

                //check that the other two cells are filled by the same player
                if (row == 0) {
                    if ((gameBoard.gameBoard[1][column] == gameBoard.gameBoard[2][column]) && (gameBoard.gameBoard[2][column] != 0)) {
                        coordinates[0] = row;
                        coordinates[1] = column;
                        //return coordinates;
                        break;
                    }
                }
                if (row == 1) {
                    if ((gameBoard.gameBoard[0][column] == gameBoard.gameBoard[2][column]) && (gameBoard.gameBoard[2][column] != 0)) {
                        coordinates[0] = row;
                        coordinates[1] = column;
                        //return coordinates;
                        break;
                    }
                }
                if (row == 2) {
                    if ((gameBoard.gameBoard[1][column] == gameBoard.gameBoard[0][column]) && (gameBoard.gameBoard[0][column] != 0)) {
                        coordinates[0] = row;
                        coordinates[1] = column;
                        //return coordinates;
                        break;
                    }
                }
            }
        }

        //if this row is not filled on 1/3rd by the current player return the default value of {-1, -1}
        return coordinates;
    }
}
