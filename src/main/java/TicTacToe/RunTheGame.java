package TicTacToe;

import java.util.Scanner;

public class RunTheGame {

    //make the instance of Scanner as a class variable so that we don't have to create a new instance for every method we use it in
    private Scanner scan = new Scanner(System.in);

    //parameters:   the various user interfaces and the user input from the terminal
    //return:   runs the game until the user wants to quit
    void runGameInLoop() {

        //repeat the loop until the user say he/she doesn't want to play again
        while (true) {

            //start the game

            commandUI();

            //ask the user if the game should run again
            String answer;
            do {
                System.out.println("\nDo you want to play again? (yes/no)");
                Scanner scan = new Scanner(System.in);
                answer = scan.next();

                //if the user respected the input rules break the loop
            } while (!answer.toLowerCase().equals("yes") && !answer.toLowerCase().equals("no"));

            //stop the game if the answer is no and repeat if the answer is yes
            if (answer.toLowerCase().equals("yes")) {
                System.out.println("\nOne more round\n");
            } else {
                if (answer.toLowerCase().equals("no")) {
                    System.out.println("\nGoodbye");
                    break;
                }
            }
        }
    }

    //parameters:   ask the user for a desired type of game
    //return:   runs the specific game
    private void commandUI() {

        //sout the instructions and specify the player characteristics
        Player[] players = chooseGameType();

        Player player1 = players[0];
        Player player2 = players[1];

        //-----------------------------------------------------------

        System.out.println("\nLet the game begin!\n");

        //create the game board object
        GameBoard game = new GameBoard();

        //whose turn is now?
        boolean turnPlayer1 = true;

        //run until someone wins
        int gameStatus;
        do {

            //show the game board
            game.showGameBoard();

            //let the player make the turn
            if (turnPlayer1) {  //player 1 plays
                turnPlayer1 = false;

                //player 1 move
                player1.makeMove(game);

            } else {    //player 2 plays
                turnPlayer1 = true;

                //player 2 move
                player2.makeMove(game);

            }

            //check if the game is finished and sout the results
            gameStatus = game.checkGameBoard(game.gameBoard);

            switch (gameStatus) {
                case 1:
                    game.showGameBoard();
                    System.out.println("\nPlayer \"X\" won\n");
                    break;

                case 2:
                    game.showGameBoard();
                    System.out.println("\nPlayer \"O\" won\n");
                    break;

                case -1:
                    game.showGameBoard();
                    System.out.println("\nThis time it is a tie.\n");
                    break;
            }

        } while (gameStatus == 0);
    }

    //------------------------------------------------------------------------------------------------------------------
    private Player[] chooseGameType() {
        Player[] players = {null, null};

        //player 1
        System.out.println("Choose the first player");
        System.out.println("1. Person\n2. PC (easy)\n3. PC (hard)\n4. PC (impossible)\n5. PC (MiniMax)");
        System.out.print("Player X: ");
        players[0] = assignPlayer(1);


        //player 2
        System.out.println("\nChoose the second player");
        System.out.println("1. Person\n2. PC (easy)\n3. PC (hard)\n4. PC (impossible)\n5. PC (MiniMax)");
        System.out.print("Player O: ");
        players[1] = assignPlayer(2);

        return players;
    }

    //------------------------------------------------------------------------------------------------------------------
    private Player assignPlayer(int playerNumber) {
        //take user input for type of player
        int userChoice;
        do {    //make sure the user chose a viable number
            userChoice = scan.nextInt();
        } while (userChoice < 1 || userChoice > 5);

        //create the player object corresponding to the user choice
        Player player;
        switch (userChoice) {
            case 1:
                player = new Person(playerNumber);
                break;
            case 2:
                player = new PcEasy(playerNumber);
                break;
            case 3:
                player = new PcHard(playerNumber);
                break;
            case 4:
                player = new PcImpossible(playerNumber);
                break;
            case 5:
                player = new MiniMax(playerNumber);
                break;
            default:
                player = new Person(playerNumber);
                break;
        }

        return player;
    }
}